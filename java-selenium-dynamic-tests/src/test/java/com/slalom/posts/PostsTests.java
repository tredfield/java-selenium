package com.slalom.posts;

import com.slalom.pages.ListPosts;
import com.slalom.pages.ListPostsPage;
import com.slalom.pages.PageType;
import com.slalom.test.base.WordpressTests;
import com.slalom.workflows.PostCreator;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static com.slalom.util.PageHelper.getPage;

/** These tests can not be ran in parallel due to the post count being verified in {@link #createdPostsShows()}. **/
public class PostsTests extends WordpressTests
{
    // Test demonstrates storing state by storing count of posts
    // Also demonstrates use of a workflow PageCreator
    @Test
    public void createdPostsShows()
    {
        // Go to posts, get current count
        ListPostsPage listPostsPage = getPage(ListPostsPage.class);
        listPostsPage.goTo(PageType.Post);
        listPostsPage.storeCount();

        // Create a new post
        PostCreator.createPost();

        // Go to posts, get count
        listPostsPage.goTo(PageType.Post);
        Assert.assertEquals("Post counts are not equal", listPostsPage.getPreviousPostCount() + 1, listPostsPage.getCurrentPostCount());

        // Check for added post
        Assert.assertTrue(listPostsPage.doesPostExistWithTitle(PostCreator.getPreviousTitle()));

        // Delete post and verify
        listPostsPage.trashPost(PostCreator.getPreviousTitle());
        Assert.assertEquals("Post count incorrect", listPostsPage.getPreviousPostCount(), listPostsPage.getCurrentPostCount());
    }

    // Demonstrate wrapping page object instance with static methods
    @Test
    @Ignore
    public void createdPostsShowsStatic()
    {
        // Go to posts, get current count
        ListPosts.storeCount();

        // Create a new post
        PostCreator.createPost();

        // Go to posts, get count
        Assert.assertEquals("Post counts are not equal", ListPosts.getPreviousPostCount() + 1, ListPosts.getCurrentPostCount());

        // Check for added post
        Assert.assertTrue(ListPosts.doesPostExistWithTitle(PostCreator.getPreviousTitle()));

        // Delete post and verify
        ListPosts.trashPost(PostCreator.getPreviousTitle());
        Assert.assertEquals("Post count incorrect", ListPosts.getPreviousPostCount(), ListPosts.getCurrentPostCount());
    }

    @Test
    public void canSearchPosts()
    {
        // Create new post
        PostCreator.createPost();

        // Search for post
        ListPosts.searchForPost(PostCreator.getPreviousTitle());

        // Check that posts shows in results
        Assert.assertTrue(ListPosts.doesPostExistWithTitle(PostCreator.getPreviousTitle()));
    }
}
