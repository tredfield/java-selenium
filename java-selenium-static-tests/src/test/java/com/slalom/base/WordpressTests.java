package com.slalom.base;

import com.slalom.pages.LoginPage;
import com.slalom.workflows.PostCreator;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import static com.slalom.selenium.TestConfiguration.Password;
import static com.slalom.selenium.TestConfiguration.User;

public class WordpressTests extends TestBase
{
    @Before
    public void setup()
    {
        PostCreator.initialize();
        LoginPage.goTo();
        LoginPage.loginAs(User.toString()).withPassword(Password.toString()).login();
    }

    @After
    public void tearDown()
    {
        PostCreator.close();
    }

    @Override
    public WebDriver getDriver()
    {
        return null;
    }
}
