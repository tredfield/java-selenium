package com.slalom.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Helper class for obtaining environment property values
 */
public final class PropertyHelper
{
    private PropertyHelper() {}

    /**
     * Retrieve property, first by Java property, second by environment
     * @param propertyName
     * @return
     */
    public static String getProperty(String propertyName)
    {
        // default to Java property
        String result = System.getProperty(propertyName);

        if (result == null)
        {
            String envProperty = StringUtils.replaceChars(propertyName, '.', '_');
            result = System.getenv(envProperty);
        }

        return result;
    }
}
