package com.slalom.util;

import com.slalom.selenium.Driver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

import static com.slalom.selenium.TestConfiguration.ScreenshotDirectory;

public final class ScreenshotHelper
{
    private ScreenshotHelper () {}

    public static void takeScreenshot(String screenshotName)
    {
        if (Driver.getDriver() instanceof TakesScreenshot)
        {
            File tempFile = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.FILE);

            try
            {
                FileUtils.copyFile(tempFile, new File(ScreenshotDirectory + "/" + screenshotName + ".png"));
            }
            catch (IOException e)
            {
                // TODO handle exception
            }
        }
    }
}
