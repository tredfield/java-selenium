package com.slalom.selenium;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RemoteCapabilities
{
    private String browser = "chrome";
    private String os;
    private String version;
    private String methodName;
    private String buildTag;

    public RemoteCapabilities withOs(String os)
    {
        this.os = os;
        return this;
    }

    public RemoteCapabilities withBrowser(String browser)
    {
        this.browser = browser;
        return this;
    }

    public RemoteCapabilities withVersion(String version)
    {
        this.version = version;
        return this;
    }

    public RemoteCapabilities withBuildTag(String buildTag)
    {
        this.buildTag = buildTag;
        return this;
    }

    public RemoteCapabilities withMethodName(String methodName)
    {
        this.methodName = methodName;
        return this;
    }

    public DesiredCapabilities build()
    {
        DesiredCapabilities driverCapabilities = new DesiredCapabilities();

        // sauce labs requires a browser, default to chrome
        driverCapabilities.setCapability(CapabilityType.BROWSER_NAME, browser);

        if (version != null) driverCapabilities.setCapability(CapabilityType.VERSION, version);
        if (os != null) driverCapabilities.setCapability(CapabilityType.PLATFORM, os);
        if (methodName != null) driverCapabilities.setCapability("name", methodName);
        if (buildTag != null) driverCapabilities.setCapability("build", buildTag);

        return driverCapabilities;
    }
}
