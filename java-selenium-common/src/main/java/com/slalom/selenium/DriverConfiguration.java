package com.slalom.selenium;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.remote.DesiredCapabilities;

import static com.slalom.util.PropertyHelper.getProperty;
import static org.openqa.selenium.phantomjs.PhantomJSDriverService.PHANTOMJS_CLI_ARGS;
import static org.openqa.selenium.phantomjs.PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY;

public enum DriverConfiguration
{
    /** Configuration java properties with default values **/
    Driver(getProperty("test.driver"), "ChromeDriver"),
    ChromeDriverLocation(getProperty("test.driver.chrome.location"), "../drivers/chromedriver"),
    PhantomJsLocation(getProperty("test.driver.phantomjs.location"), "../drivers/phantomjs"),
    ImplicitWait(getProperty("test.driver.implicit.wait.milliseconds"), "0"),
    RemoteBrowser(getProperty("test.driver.remote.browser"), "Chrome"),
    RemoteURL(getProperty("test.driver.remote.url"), "http://localhost:4444/wd/hub"),
    UseSauceLabs(getProperty("test.driver.use.sauce.labs"), "false");

    private final String value;
    private final String defaultValue;

    DriverConfiguration(String value, String defaultValue)
    {
        this.value = value;
        this.defaultValue = defaultValue;
    }

    public String getValue()
    {
        return this.value;
    }

    @Override
    public String toString()
    {
        return StringUtils.defaultIfBlank(value, defaultValue);
    }

    public static DriverSelection getDriverSelection() { return DriverSelection.valueOf(Driver.toString()); }

    static RemoteBrowsers getRemoteBrowser() { return RemoteBrowsers.valueOf(RemoteBrowser.toString()); }

    public enum DriverSelection
    {
        ChromeDriver, Safari, PhantomJs, Remote, RemoteConcurrent
    }

    enum RemoteBrowsers
    {
        Chrome(DesiredCapabilities.chrome()),
        Safari(DesiredCapabilities.safari()),
        PhantomJs(getPhantomJsCapabilities()),
        Firefox(DesiredCapabilities.firefox()),
        InternetExplorer(DesiredCapabilities.internetExplorer());

        static DesiredCapabilities getPhantomJsCapabilities()
        {
            DesiredCapabilities phantomCaps = DesiredCapabilities.phantomjs();
            phantomCaps.setCapability(PHANTOMJS_EXECUTABLE_PATH_PROPERTY, PhantomJsLocation.toString());
            phantomCaps.setCapability(PHANTOMJS_CLI_ARGS, new String[] { "--cookies-file=/tmp/cookies.txt" });
            return phantomCaps;
        }
    
        private final DesiredCapabilities capabilities;
    
        RemoteBrowsers(DesiredCapabilities desiredCapabilities)
        {
            this.capabilities = desiredCapabilities;
        }

        public DesiredCapabilities getCapabilities()
        {
            return capabilities;
        }
    }
}
