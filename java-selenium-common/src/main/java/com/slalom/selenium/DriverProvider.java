package com.slalom.selenium;

import org.openqa.selenium.WebDriver;

public interface DriverProvider
{
    WebDriver getDriver();
}
