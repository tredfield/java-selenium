package com.slalom.selenium;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;

// This probably should be in the framework projects but putting here so it can be shared between static and dynamic
// frameworks
public enum TestConfiguration
{
    /** Configuration java properties with default values **/
    BaseAddress(System.getProperty("test.site.address"), "http://localhost:8888"),
    User(System.getProperty("test.site.user"), "admin"),
    Password(System.getProperty("test.site.password"), "root"),
    ScreenshotDirectory(System.getProperty("test.screenshot.output"), "screenshots"),
    Browsers(System.getProperty("test.browsers"), "chrome");

    private final String value;
    private final String defaultValue;

    TestConfiguration(String value, String defaultValue)
    {
        this.value = value;
        this.defaultValue = defaultValue;
    }

    @Override
    public String toString()
    {
        return StringUtils.defaultIfBlank(value, defaultValue);
    }

    /** Provide the browsers as parameter list for testing against **/
    public static LinkedList<String[]> browsersStrings()
    {
        LinkedList<String[]> result = new LinkedList<>();
        String[] browserList = StringUtils.split(Browsers.toString(), ",");

        for (String each : browserList)
        {
            result.add(new String[] {StringUtils.trim(each)});
        }

        return result;
    }
}
