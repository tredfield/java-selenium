package com.slalom.selenium;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.slalom.selenium.DriverConfiguration.RemoteBrowsers.getPhantomJsCapabilities;
import static com.slalom.selenium.DriverConfiguration.*;

public final class Driver
{
    private Driver() {}

    private final static ThreadLocal<WebDriver> INSTANCE = new ThreadLocal<>();

    public static WebDriver getDriver()
    {
        return INSTANCE.get();
    }

    public static void initialize(DriverProvider driverProvider)
    {
        if (INSTANCE.get() == null)
        {
            INSTANCE.set(initDriver(driverProvider));
            turnOnWait();
        }
    }

    private static WebDriver initDriver(DriverProvider driverProvider)
    {
        switch (getDriverSelection())
        {
            case Safari:
                return new SafariDriver();
            case PhantomJs:
                return getPhantomJs();
            case Remote:
                return getRemoteDriver();
            case RemoteConcurrent:
                WebDriver driver = driverProvider.getDriver();
                return driver != null ? driver : getRemoteDriver();
            default:
                return getChrome();
        }
    }

    public static void close()
    {
        INSTANCE.get().quit();
        INSTANCE.set(null);
    }

    // TODO improve this wait
    public static void wait(int milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public static void noWait(Runnable func)
    {
        turnOffWait();
        func.run();
        turnOnWait();
    }

    private static void turnOnWait()
    {
        getDriver().manage().timeouts().implicitlyWait(
                Integer.parseInt(ImplicitWait.toString()), TimeUnit.MILLISECONDS);
    }

    private static void turnOffWait()
    {
        getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
    }

    private static WebDriver getChrome()
    {
        System.setProperty("webdriver.chrome.driver", ChromeDriverLocation.toString());
        return new ChromeDriver();
    }

    private static WebDriver getPhantomJs()
    {
        WebDriver driver = new PhantomJSDriver(getPhantomJsCapabilities());
        driver.manage().window().setSize(new Dimension(1600, 1200));
        return driver;
    }

    private static WebDriver getRemoteDriver()
    {
        URL remoteUrl;

        try
        {
            remoteUrl = new URL(RemoteURL.toString());
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException("Invalid remote URL: " + RemoteURL);
        }

        return new RemoteWebDriver(remoteUrl, getRemoteBrowser().getCapabilities());
    }
}
