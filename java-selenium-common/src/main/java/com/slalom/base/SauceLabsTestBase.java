package com.slalom.base;

import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.junit.SauceOnDemandTestWatcher;
import com.slalom.selenium.Driver;
import com.slalom.selenium.RemoteCapabilities;
import com.slalom.util.SauceHelper;
import org.junit.Before;
import org.junit.Rule;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import static com.slalom.selenium.DriverConfiguration.RemoteURL;
import static com.slalom.selenium.DriverConfiguration.UseSauceLabs;

public class SauceLabsTestBase extends TestBase implements SauceOnDemandSessionIdProvider
{
    private static final String username = System.getProperty("test.sauce.user");
    private static final String accesskey = System.getProperty("test.sauce.access.key");
    private static final String seleniumURI = SauceHelper.buildSauceUri();
    private static final boolean useSauceLabs =  Boolean.valueOf(UseSauceLabs.toString());

    public SauceLabsTestBase()
    {
        if (useSauceLabs)
        {
            authentication = new SauceOnDemandAuthentication(username, accesskey);
            resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);
            resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);
        }
    }

    /**
     * Remote web driver capabilities
     */
    protected RemoteCapabilities capabilities = new RemoteCapabilities();

    /**
     * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
     * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
     */
    public SauceOnDemandAuthentication authentication;

    /**
     * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
     */
    @Rule
    public SauceOnDemandTestWatcher resultReportingTestWatcher;

    /**
     * Instance variable which contains the Sauce Job Id.
     */
    protected String sessionId;

    /**
     * Constructs a new {@link RemoteWebDriver} instance which is configured to use the capabilities defined by the
     * {@link RemoteCapabilities#browser}, {@link RemoteCapabilities#version} and {@link RemoteCapabilities#os}
     * values, and which is configured to run against ondemand.saucelabs.com, using
     * the username and access key populated by the {@link #authentication} instance.
     *
     * @throws Exception if an error occurs during the creation of the {@link RemoteWebDriver} instance.
     */
    @Before
    public void sauceSetup()
    {
        this.sessionId = (((RemoteWebDriver) Driver.getDriver()).getSessionId()).toString();
        String message = String.format("SauceOnDemandSessionID=%1$s job-name=%2$s", this.sessionId, name.getMethodName());
        System.out.println(message);
    }

    /**
     *
     * @return the value of the Sauce Job id.
     */
    @Override
    public String getSessionId()
    {
        return sessionId;
    }

    protected DesiredCapabilities getCapabilities()
    {
        DesiredCapabilities desiredCapabilities = capabilities.withMethodName(name.getMethodName())
                .withBuildTag(buildTag).build();
        SauceHelper.addSauceConnectTunnelId(desiredCapabilities);
        return desiredCapabilities;
    }

    protected String getUrlString()
    {
        return useSauceLabs ? "http://" + username + ":" + accesskey + seleniumURI +"/wd/hub"
                : RemoteURL.toString();
    }

    @Override
    public WebDriver getDriver()
    {
        return new RemoteWebDriver(getUrl(), getCapabilities());
    }

    private URL getUrl()
    {
        String urlString = getUrlString();

        try
        {
            return new URL(urlString);
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException("Invalid remote URL: " + urlString);
        }
    }
}
