package com.slalom.base;

import com.saucelabs.junit.ConcurrentParameterized;
import org.junit.runner.RunWith;

@RunWith(ConcurrentParameterized.class)
public class ConcurrentParameterizedTestBase extends SauceLabsTestBase
{
    public ConcurrentParameterizedTestBase(String browser)
    {
        super();
        capabilities.withBrowser(browser);
    }
}
