package com.slalom.workflows;

import com.slalom.pages.ListPostsPage;
import com.slalom.pages.NewPostPage;
import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;

import static com.slalom.util.PageHelper.getPage;

public abstract class PostCreator
{
    private static final ThreadLocal<PostData> threadPostData = new ThreadLocal<>();

    public static void initialize()
    {
        threadPostData.set(null);
    }

    public static void close()
    {
        if (createdPost())
        {
            trashPost();
        }
    }

    public static String getPreviousTitle()
    {
        PostData data = threadPostData.get();
        return data != null ? data.title : null;
    }

    private static String getPreviousBody()
    {
        PostData data = threadPostData.get();
        return data != null ? data.body : null;
    }

    public static void createPost()
    {
        threadPostData.set(new PostData());
        getPage(NewPostPage.class).goTo()
                .createPost(getPreviousTitle()).withBody(getPreviousBody()).publish();
    }

    private static void trashPost()
    {
        getPage(ListPostsPage.class).trashPost(getPreviousTitle());
        initialize();
    }

    private static boolean createdPost()
    {
        return StringUtils.isNoneEmpty(getPreviousTitle());
    }

    private static class PostData
    {
        String title = "Title from automated test " + Calendar.getInstance().getTime();
        String body = "Body from automated test " + Calendar.getInstance().getTime();
    }
}
