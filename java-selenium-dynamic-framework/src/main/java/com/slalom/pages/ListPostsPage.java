package com.slalom.pages;

import com.slalom.elements.PostRows;
import com.slalom.navigation.LeftNavigation;
import com.slalom.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static com.slalom.util.PageHelper.getDriver;
import static com.slalom.util.PageHelper.getPage;

public class ListPostsPage extends BasePage
{
    private int lastCount;

    @FindBy(className="displaying-num")
    private WebElement countText;

    @FindBy(id="post-search-input")
    private WebElement searchBox;

    @FindBy(id="search-submit")
    private WebElement searchButton;

    @FindBy(tagName="h1")
    private List<WebElement> h1s;


    public static void goTo(PageType type) throws RuntimeException
    {
        switch (type)
        {
            case Page:
                LeftNavigation.Pages.AllPages.select();
                break;
            case Post:
                LeftNavigation.Posts.AllPosts.select();
                break;
            default:
                throw new RuntimeException("Unknown PageType " + type.toString());
        }
    }
    
    public void storeCount()
    {
        lastCount = getPostCount();
    }
    
    private int getPostCount()
    {
        return Integer.parseInt(countText.getText().split(" ")[0]);
    }
    
    public void trashPost(String title)
    {
        getPage(PostRows.class).deletePost(title);
    }
    
    public int getPreviousPostCount()
    {
        return this.lastCount;
    }

    public int getCurrentPostCount()
    {
        return this.getPostCount();
    }
    
    public boolean doesPostExistWithTitle(String title)
    {
        return getDriver().findElements(By.linkText(title)).size() > 0;
    }
    
    public void searchForPost(String title)
    {
        if (!isAt())
        {
            goTo(PageType.Post);
        }

        searchBox.sendKeys(title);
        searchButton.click();
    }

    public boolean isAt()
    {
        if (h1s.size() > 0)
        {
            return "Posts".equals(h1s.get(0).getText());
        }

        return false;
    }
}
