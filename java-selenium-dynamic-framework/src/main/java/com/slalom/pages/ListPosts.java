package com.slalom.pages;

import static com.slalom.util.PageHelper.getPage;

public final class ListPosts
{
    private ListPosts() {}

    private static final ThreadLocal<ListPostsPage> PAGE = new ThreadLocal<>();

    public static void close()
    {
        PAGE.set(null);
    }

    private static ListPostsPage page()
    {
        if (PAGE.get() == null)
        {
            PAGE.set(getPage(ListPostsPage.class));
        }

        return PAGE.get();
    }

    private static void goToPostsPage()
    {
        if (!page().isAt())
            ListPostsPage.goTo(PageType.Post);
    }

    public static void storeCount()
    {
        goToPostsPage();
        page().storeCount();
    }

    public static int getPreviousPostCount()
    {
        goToPostsPage();
        return page().getPreviousPostCount();
    }

    public static boolean doesPostExistWithTitle(String title)
    {
        return page().doesPostExistWithTitle(title);
    }

    public static void trashPost(String title)
    {
        page().trashPost(title);
    }

    public static int getCurrentPostCount()
    {
        goToPostsPage();
        return page().getCurrentPostCount();
    }

    public static void searchForPost(String title)
    {
        page().searchForPost(title);
    }
}
