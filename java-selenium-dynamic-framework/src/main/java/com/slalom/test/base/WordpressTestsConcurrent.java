package com.slalom.test.base;

import com.saucelabs.junit.ConcurrentParameterized;
import com.slalom.base.ConcurrentParameterizedTestBase;
import com.slalom.pages.ListPosts;
import com.slalom.pages.LoginPage;
import com.slalom.selenium.TestConfiguration;
import com.slalom.workflows.PostCreator;
import org.junit.After;
import org.junit.Before;

import java.util.LinkedList;

import static com.slalom.selenium.TestConfiguration.Password;
import static com.slalom.selenium.TestConfiguration.User;
import static com.slalom.util.PageHelper.getPage;

public class WordpressTestsConcurrent extends ConcurrentParameterizedTestBase
{
    public WordpressTestsConcurrent(String browser)
    {
        super(browser);
    }

    @Before
    public void setupBefore()
    {
        PostCreator.initialize();
        getPage(LoginPage.class).goTo().loginAs(User.toString()).withPassword(Password.toString()).login();
    }

    @After
    public void closeAfter()
    {
        PostCreator.close();
        ListPosts.close();
    }

    @ConcurrentParameterized.Parameters()
    public static LinkedList<String[]> browsersStrings()
    {
        return TestConfiguration.browsersStrings();
    }
}
