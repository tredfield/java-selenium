package com.slalom.test.base;

import com.slalom.base.SauceLabsTestBase;
import com.slalom.pages.ListPosts;
import com.slalom.pages.LoginPage;
import com.slalom.workflows.PostCreator;
import org.junit.After;
import org.junit.Before;

import static com.slalom.selenium.TestConfiguration.Password;
import static com.slalom.selenium.TestConfiguration.User;
import static com.slalom.util.PageHelper.getPage;

public class WordpressTests extends SauceLabsTestBase
{
    @Before
    public void setupBefore()
    {
        PostCreator.initialize();
        getPage(LoginPage.class).goTo().loginAs(User.toString()).withPassword(Password.toString()).login();
    }

    @After
    public void closeAfter()
    {
        PostCreator.close();
        ListPosts.close();
    }
}
