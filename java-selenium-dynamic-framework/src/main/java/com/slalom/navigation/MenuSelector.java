package com.slalom.navigation;

import com.slalom.selenium.Driver;
import org.openqa.selenium.By;

import static com.slalom.util.PageHelper.getDriver;

public abstract class MenuSelector
{
    public static void select(String topLevelMenu, String subMenuLinkText)
    {
        getDriver().findElement(By.id(topLevelMenu)).click();
        // TODO implement better wait
        Driver.wait(250);
        getDriver().findElement(By.linkText(subMenuLinkText)).click();
    }
}
