package com.slalom.workflows;

import com.slalom.pages.ListPostsPage;
import com.slalom.pages.NewPostPage;
import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;

public class PostCreator
{
    private static String previousTitle;
    private static String previousBody;
    
    public static String getPreviousTitle()
    {
        return previousTitle;
    }

    public static void createPost()
    {
        previousTitle = createTitle();
        previousBody = createBody();
        NewPostPage.goTo();
        NewPostPage.createPost(getPreviousTitle()).withBody(previousBody).publish();
    }

    private static String createTitle()
    {
        return "Title from automated test " + Calendar.getInstance().getTime();
    }

    private static String createBody()
    {
        return "Body from automated test " + Calendar.getInstance().getTime();
    }

    public static void initialize()
    {
        previousTitle = null;
        previousBody = null;
    }

    public static void close()
    {
        if (createdPost())
        {
            trashPost();
        }
    }

    private static void trashPost()
    {
        ListPostsPage.trashPost(previousTitle);
        initialize();
    }

    private static boolean createdPost()
    {
        return StringUtils.isNoneEmpty(previousTitle);
    }
}
