package com.slalom.pages;

import com.google.common.base.Predicate;
import com.slalom.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.slalom.selenium.TestConfiguration.BaseAddress;


public class LoginPage
{
    public static void goTo()
    {
        Driver.getDriver().navigate().to(BaseAddress + "/wp-login.php");
        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 5);
        wait.until((Predicate<WebDriver>) p -> p.switchTo().activeElement().getAttribute("id").equals("user_login") );
    }

    public static LoginCommand loginAs(String user)
    {
        return new LoginCommand(user);
    }

    public static class LoginCommand
    {
        private final String user;
        private String password;
    
        public LoginCommand(String user)
        {
            this.user = user;
        }

        public LoginCommand withPassword(String pass)
        {
            this.password = pass;
            return this;
        }

        public LoginCommand login()
        {
            WebElement userInput = Driver.getDriver().findElement(By.id("user_login"));
            userInput.sendKeys(this.user);

            WebElement passInput = Driver.getDriver().findElement(By.id("user_pass"));
            passInput.sendKeys(this.password);

            WebElement loginButton = Driver.getDriver().findElement(By.id("wp-submit"));
            loginButton.click();

            return this;
        }
    }
}
