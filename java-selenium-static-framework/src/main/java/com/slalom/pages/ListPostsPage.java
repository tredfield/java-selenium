package com.slalom.pages;

import com.slalom.navigation.LeftNavigation;
import com.slalom.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;

public class ListPostsPage
{
    private static int lastCount = 0;

    private static boolean isAt()
    {
        List<WebElement> h1s = Driver.getDriver().findElements(By.tagName("h1"));
        return h1s.size() > 0 && "Posts".equals(h1s.get(0).getText());
    }

    public static void searchForPost(String title)
    {
        if (!isAt())
        {
            goTo(PageType.Post);
        }

        WebElement searchBox = Driver.getDriver().findElement(By.id("post-search-input"));
        searchBox.sendKeys(title);

        WebElement searchButton = Driver.getDriver().findElement(By.id("search-submit"));
        searchButton.click();
    }
    
    public static void goTo(PageType type) throws RuntimeException
    {
        switch (type)
        {
            case Page:
                LeftNavigation.Pages.AllPages.select();
                break;
            case Post:
                LeftNavigation.Posts.AllPosts.select();
                break;
            default:
                throw new RuntimeException("Unknown PageType " + type.toString());
        }
    }
    
    public static boolean doesPostExistWithTitle(String title)
    {
        return Driver.getDriver().findElements(By.linkText(title)).size() > 0;
    }

    public static void trashPost(String title)
    {
        List<WebElement> rows = Driver.getDriver().findElements(By.tagName("tr"));

        for (WebElement row : rows)
        {
            List<WebElement> links = new ArrayList<>();
            Driver.noWait(() -> links.addAll(row.findElements(By.linkText(title))));

            if (links.size() <= 0) continue;

            Actions action = new Actions(Driver.getDriver());
            action.moveToElement(links.get(0));
            action.perform();
            row.findElement(By.className("submitdelete")).click();
            break;
        }
    }

    public static void storeCount()
    {
        lastCount = getPostCount();
    }
    
    private static int getPostCount()
    {
        String countText = Driver.getDriver().findElement(By.className("displaying-num")).getText();
        return Integer.parseInt(countText.split(" ")[0]);
    }
    
    public static int getPreviousPostCount()
    {
        return lastCount;
    }
    
    public static int getCurrentPostCount()
    {
        return getPostCount();
    }
}
