package com.slalom.pages;

import com.slalom.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DashboardPage
{
    public static boolean isAt()
    {
        List<WebElement> h1s = Driver.getDriver().findElements(By.tagName("h1"));
        return h1s.size() > 0 && h1s.get(0).getText().equals("Dashboard");
    }
}
